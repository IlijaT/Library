var knjizaraApp = angular.module('knjizaraApp',  ['ngRoute']);

knjizaraApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : '/app/html/partial/home.html'
		})
		.when('/knjige', {
			templateUrl : '/app/html/partial/knjige.html'
		})
		.when('/izdavaci', {
			templateUrl : '/app/html/partial/izdavaci.html'
		})
		.when('/about', {
			templateUrl : '/app/html/partial/about.html'
		})
		.when('/hours', {
			templateUrl : '/app/html/partial/store.html'
		})
		.when('/knjige/izmena/:kid', { 
			templateUrl : '/app/html/partial/izmenaKnjige.html'
		})
		.otherwise({
			redirectTo: '/'
		});
}]);

knjizaraApp.controller("knjigeCtrl", function($scope,$http, $location){
	
	
	var baseUrl = "/api/knjige";
	
	$scope.knjige = [];
	$scope.izdavaci = [];
	
	$scope.novaKnjiga = {};
	$scope.novaKnjiga.naziv = "";
	$scope.novaKnjiga.izdanje = "";
	$scope.novaKnjiga.pisac = "";
	$scope.novaKnjiga.ISBN = "";
	$scope.novaKnjiga.izdavacId = "";
	$scope.novaKnjiga.izdavacNaziv = "";
	
	$scope.trazenaKnjiga = {};
	$scope.trazenaKnjiga.naziv = "";
	$scope.trazenaKnjiga.pisac = "";
	$scope.trazenaKnjiga.brojGlasova = "";
	
	$scope.pageNum = 0;
	$scope.totalPages = 1;
	
	var getKnjige = function(){
		
		
		var config = { params: {}};
		
		if($scope.trazenaKnjiga.naziv != ""){
			config.params.naziv = $scope.trazenaKnjiga.naziv;
		}
		if($scope.trazenaKnjiga.pisac != ""){
			config.params.pisac = $scope.trazenaKnjiga.pisac;
		}
		if($scope.trazenaKnjiga.brojGlasova != ""){
			config.params.brojGlasova = $scope.trazenaKnjiga.brojGlasova;
		}
		
		config.params.pageNum = $scope.pageNum;
		
		var promise = $http.get(baseUrl,  config);
		promise.then(
				function uspeh(data){
					$scope.totalPages = data.headers("totalPages");
					$scope.knjige = data.data;
					$scope.trazenaKnjiga = {};
				},
				function neuspeh(data){
					alert("Neuspesno dobavljanje podataka.");
				}
		);
	}
	
	var getIzdavaci = function(){
		
		var promise = $http.get("/api/izdavaci");
		promise.then(
				function uspeh(data){
					$scope.totalPages = data.headers("totalPages");
					$scope.izdavaci = data.data;
				},
				function neuspeh(data){
					alert("Neuspesno dobavljanje podataka.");
				}
		);
	}
	
	$scope.najpopularnije = [];
	
	var getNajpopularnija = function(){
		
		var promise = $http.get("/api/knjige/najpopularnija");
		promise.then(
				function uspeh(data){
					$scope.najpopularnije = data.data;
					getKnjige();
				},
				function neuspeh(data){
					alert("Neuspesno dobavljanje podataka.");
				}
		);
	}
	
	getIzdavaci();
	getKnjige();
	getNajpopularnija();
	
	$scope.addKnjiga = function(){
		$http.post("/api/knjige", $scope.novaKnjiga).then(
			function success(data){
				getKnjige();
				$scope.novaKnjiga = {};
			},
			function error(data){
				alert("Neuspesan pokusaj dodavanja");
			}
		)
	}
	
	$scope.Glas = {};
	
	
	$scope.glasaj = function(knjigaId){
		$scope.Glas.knjigaId = knjigaId;
	 
			$http.post(baseUrl + "/glasaj", $scope.Glas).then(
				function success(data){
					getKnjige();
					$scope.Glas = {};
					getNajpopularnija();
				},
				function error(data){
					alert("Neuspesno dodavanje knjiga.");
				}
			)
		}

	
	$scope.izmena = function(knjigaId){
		$location.path("/knjige/izmena/" + knjigaId);
	}
	
	$scope.page = function(direction){
		$scope.pageNum = $scope.pageNum + direction;
		getKnjige();
	}
	
	
	$scope.filtriranje = function(){
		$scope.pageNum = 0;
		getKnjige();
	}
	
	
 
 
	
	$scope.deleteKnjiga = function(knjigaId){
		
		var promise = $http.delete(baseUrl + "/" + knjigaId);
		promise.then(
			function uspeh(data){
				getKnjige();
			},
			function neuspeh(data){
				alert("Nije uspelo brisanje knjige!");
			}
		);
	}
	
});



knjizaraApp.controller("izmenaCtrl", function($scope, $http, $routeParams, $location){
	
	var id = $routeParams.kid;
	var baseUrl = "/api/knjige";
	
	$scope.izdavaci = [];
	
	$scope.staraKnjiga = {};
	$scope.staraKnjiga.naziv = "";
	$scope.staraKnjiga.izdanje = "";
	$scope.staraKnjiga.pisac = "";
	$scope.staraKnjiga.ISBN = "";
	$scope.staraKnjiga.izdavacId = "";
	$scope.staraKnjiga.izdavacNaziv = "";
 
	$scope.pageNum = 0;
	$scope.totalPages = 1;
	
	var getStaraKnjiga = function(){
		
		var promise = $http.get(baseUrl + "/" + id);
		promise.then(
			function success(obj){
				$scope.staraKnjiga = obj.data;
			},
			function error(obj){
				alert("Neuspesno dobavljanje stare knjige.");
			}
		);
	}
	
	var getIzdavaci = function(){
		
		var promise = $http.get("/api/izdavaci");
		promise.then(
				function uspeh(data){
					$scope.totalPages = data.headers("totalPages");
					$scope.izdavaci = data.data;
				},
				function neuspeh(data){
					alert("Neuspesno dobavljanje podataka.");
				}
		);
	}
	
	getIzdavaci();
	getStaraKnjiga();
	
	$scope.izmeniKnjigu = function(){
		$http.put("/api/knjige/" + id, $scope.staraKnjiga).then(
				function success(data){
					$location.path("/knjige");
				},
				function error(data){
					alert("Neuspela izmena knjige.");
				}
			)};
	
	
	
});