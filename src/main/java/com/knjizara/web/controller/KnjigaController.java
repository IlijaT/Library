package com.knjizara.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.knjizara.model.Knjiga;
import com.knjizara.service.KnjigaService;
import com.knjizara.support.KnjigaDTOToKnjiga;
import com.knjizara.support.KnjigaToKnjigaDTO;
import com.knjizara.web.dto.KnjigaDTO;

@RestController
@RequestMapping("/api/knjige")
public class KnjigaController {

	@Autowired
	KnjigaService knjigaService;
	@Autowired
	KnjigaDTOToKnjiga toKnjiga;
	@Autowired
	KnjigaToKnjigaDTO toDTO;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<KnjigaDTO>> get(
			@RequestParam(required = false) String naziv,
			@RequestParam(required = false) String pisac, 
			@RequestParam(required = false) Integer brojGlasova,
			@RequestParam(defaultValue = "0") int pageNum) {

		Page<Knjiga> knjige;

		if (naziv != null || pisac != null || brojGlasova != null) {
			knjige = knjigaService.pretraga(naziv, pisac, brojGlasova, pageNum);
		} else {
			knjige = knjigaService.findAll(pageNum);
		}

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(knjige.getTotalPages()));

		return new ResponseEntity<>(toDTO.convert(knjige.getContent()), headers, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<KnjigaDTO> get(@PathVariable Long id) {

		Knjiga knjiga = knjigaService.findOne(id);

		if (knjiga == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toDTO.convert(knjiga), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/najpopularnija", method = RequestMethod.GET)
	public ResponseEntity<KnjigaDTO> get() {

		List<Knjiga> knjige = knjigaService.najpopularnija();

		if (knjige == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toDTO.convert(knjige.get(0)), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<Void> dodavanje(@RequestBody KnjigaDTO dto) {

		knjigaService.save(toKnjiga.convert(dto));

		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = "application/json")
	public ResponseEntity<KnjigaDTO> izmena(@PathVariable Long id, @RequestBody KnjigaDTO dto) {

		if(id == null || id != dto.getId()){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			
		}
		
		Knjiga knjiga = toKnjiga.convert(dto);
		knjigaService.save(knjiga);

		return new ResponseEntity<>(toDTO.convert(knjiga), HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<KnjigaDTO> brisanje(@PathVariable Long id) {

		knjigaService.remove(id);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
