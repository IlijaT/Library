package com.knjizara.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.knjizara.model.Izdavac;
import com.knjizara.model.Knjiga;
import com.knjizara.service.IzdavacService;
import com.knjizara.service.KnjigaService;
import com.knjizara.support.IzdavacDTOToIzdavac;
import com.knjizara.support.IzdavacToIzdavacDTO;
import com.knjizara.support.KnjigaToKnjigaDTO;
import com.knjizara.web.dto.IzdavacDTO;
import com.knjizara.web.dto.KnjigaDTO;

@RestController
@RequestMapping("/api/izdavaci")
public class IzdavacController {
	
	@Autowired
	IzdavacService izdavacService;
	
	@Autowired
	KnjigaService knjigaService;

	@Autowired
	IzdavacToIzdavacDTO toDTO;
	@Autowired
	IzdavacDTOToIzdavac toIzdavac;
	@Autowired
	KnjigaToKnjigaDTO toKnjigaDTO;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<IzdavacDTO>> get(@RequestParam(defaultValue = "0") int pageNum) {

		Page<Izdavac> izdavaci = izdavacService.findAll(pageNum);

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(izdavaci.getTotalPages()) );
		
		return new ResponseEntity<>(toDTO.convert(izdavaci.getContent()), headers, HttpStatus.OK);

	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<IzdavacDTO> get(@PathVariable Long id) {

		Izdavac izdavac = izdavacService.findOne(id);

		if (izdavac == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toDTO.convert(izdavac), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}/knjige", method = RequestMethod.GET)
	public ResponseEntity<List<KnjigaDTO>> get(@PathVariable Long id, @RequestParam(defaultValue = "0") int pageNum) {

		Page<Knjiga> knjige = knjigaService.findByIzdavacId(id, pageNum);
		

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(knjige.getTotalPages()) );

		return new ResponseEntity<>(toKnjigaDTO.convert(knjige.getContent()), HttpStatus.OK);
	}

}
