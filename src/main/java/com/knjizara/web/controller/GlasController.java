package com.knjizara.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.knjizara.model.Glas;
import com.knjizara.service.GlasService;
import com.knjizara.web.dto.GlasDTO;

@RestController
@RequestMapping("/api/knjige/glasaj")
public class GlasController {
	
	@Autowired
	GlasService glasService;
	
	 
	
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<Void> add(@RequestBody GlasDTO newGlas) {

		Glas glas = glasService.glasaj(newGlas.getKnjigaId());
				
		if(glas == null)
			return new ResponseEntity<>(HttpStatus.CONFLICT);

		return new ResponseEntity<>( HttpStatus.CREATED);
	}

}
