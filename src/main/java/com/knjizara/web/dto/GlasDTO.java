package com.knjizara.web.dto;

public class GlasDTO {
	
	private Long id;
	
	private String knjigaNaziv;
	
	private Long knjigaId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKnjigaNaziv() {
		return knjigaNaziv;
	}

	public void setKnjigaNaziv(String knjigaNaziv) {
		this.knjigaNaziv = knjigaNaziv;
	}

	public Long getKnjigaId() {
		return knjigaId;
	}

	public void setKnjigaId(Long knjigaId) {
		this.knjigaId = knjigaId;
	}
	
	

}
