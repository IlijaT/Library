package com.knjizara;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.knjizara.model.Izdavac;
import com.knjizara.model.Knjiga;
import com.knjizara.service.IzdavacService;
import com.knjizara.service.KnjigaService;


@Component
public class TestData {
	
	@Autowired
	private IzdavacService izdavacService;
	
	@Autowired
	private KnjigaService knjigaService;
	

	@PostConstruct
	public void init(){ 
		
		Izdavac i1 = new Izdavac();
	
		i1.setNaziv("Delfi");
		i1.setAdresa("Erika Kleptona 16, Beograd");
		i1.setTelefon("011222333");
		izdavacService.save(i1);
		
		Izdavac i2 = new Izdavac();
		i2.setNaziv("Prometej");
		i2.setAdresa("Dzimi Hendriksa 22, Novi Sad");
		i2.setTelefon("021222444");
		izdavacService.save(i2);
		
		Izdavac i3 = new Izdavac();
		i3.setNaziv("Geopoetika");
		i3.setAdresa("Boba Dilana 45, Beograd");
		i3.setTelefon("011222444");
		izdavacService.save(i3);
		
		Knjiga k1 = new Knjiga();
		k1.setNaziv("Na Drini Cuprija");
		k1.setIzdavac(i1);
		k1.setPisac("Ivo Andric");
		k1.setISBN("9781114706404");
		k1.setIzdanje(2001);
	
		knjigaService.save(k1);
		
		Knjiga k2 = new Knjiga();
		k2.setNaziv("100 Godina Samoce");
		k2.setIzdavac(i2);
		k2.setPisac("Gabrijel Garsija Markes");
		k2.setISBN("9788674706147");
		k2.setIzdanje(2009);
		
		knjigaService.save(k2);
		
		Knjiga k3 = new Knjiga();
		k3.setNaziv("Zlocin i kazna");
		k3.setIzdavac(i3);
		k3.setPisac("Fjodor M. Dostojevski");
		k3.setISBN("9788674706404");
		k3.setIzdanje(2004);
		
		knjigaService.save(k3);
		
		Knjiga k4 = new Knjiga();
		k4.setNaziv("Kandze");
		k4.setIzdavac(i3);
		k4.setPisac("Marko Vidojkovic");
		k4.setISBN("9788674706410");
		k4.setIzdanje(2015);
		
		knjigaService.save(k4);
		
		Knjiga k5 = new Knjiga();
		k5.setNaziv("Alhemicar");
		k5.setIzdavac(i1);
		k5.setPisac("Paulo Koeljo");
		k5.setISBN("9711674706410");
		k5.setIzdanje(2011);
		
		knjigaService.save(k5);
		
		Knjiga k6 = new Knjiga();
		k6.setNaziv("Lovac na zmajeve");
		k6.setIzdavac(i1);
		k6.setPisac("Haled Hoseini");
		k6.setISBN("9711674752410");
		k6.setIzdanje(2013);
		
		knjigaService.save(k6);
		
		Knjiga k7 = new Knjiga();
		k7.setNaziv("Faust");
		k7.setIzdavac(i1);
		k7.setPisac("Gete");
		k7.setISBN("9711674711410");
		k7.setIzdanje(2005);
		
		knjigaService.save(k7);
		
		Knjiga k8 = new Knjiga();
		k8.setNaziv("Kratka istorija vaseljene");
		k8.setIzdavac(i3);
		k8.setPisac("Siven Hoking");
		k8.setISBN("9713374711410");
		k8.setIzdanje(2000);
		
		knjigaService.save(k8);
		
		Knjiga k9 = new Knjiga();
		k9.setNaziv("Samarkand");
		k9.setIzdavac(i2);
		k9.setPisac("Amin Maluf");
		k9.setISBN("9713374735410");
		k9.setIzdanje(2002);
		
		knjigaService.save(k9);
		
		Knjiga k10 = new Knjiga();
		k10.setNaziv("Besnilo");
		k10.setIzdavac(i1);
		k10.setPisac("Borislav Pekic");
		k10.setISBN("9713777735410");
		k10.setIzdanje(2004);
		
		knjigaService.save(k10);
		
		Knjiga k11 = new Knjiga();
		k11.setNaziv("Tamo dole");
		k11.setIzdavac(i1);
		k11.setPisac("Bil Brajson");
		k11.setISBN("9713777711110");
		k11.setIzdanje(2011);
		
		knjigaService.save(k11);
		
		
	}
}
