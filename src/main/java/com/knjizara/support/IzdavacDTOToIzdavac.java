package com.knjizara.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.knjizara.model.Izdavac;
import com.knjizara.service.IzdavacService;
import com.knjizara.web.dto.IzdavacDTO;

@Component
public class IzdavacDTOToIzdavac implements Converter<IzdavacDTO, Izdavac> {
	
	@Autowired
	IzdavacService izdavacService;

	@Override
	public Izdavac convert(IzdavacDTO dto) {
		
		Izdavac retVal = new Izdavac();
		
		if(dto.getId() != null) {
			retVal = izdavacService.findOne(dto.getId());

			if(retVal == null) {
				throw new IllegalStateException("tried to modify a non-existant izdavac");							
			}
		}
		
		retVal.setId(dto.getId());
		retVal.setAdresa(dto.getAdresa());
		retVal.setNaziv(dto.getNaziv());
		retVal.setTelefon(dto.getTelefon());
		
		return retVal;
	}
	
	
 

}
