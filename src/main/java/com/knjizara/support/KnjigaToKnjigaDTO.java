package com.knjizara.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.knjizara.model.Knjiga;
import com.knjizara.web.dto.KnjigaDTO;

@Component
public class KnjigaToKnjigaDTO implements Converter<Knjiga, KnjigaDTO> {

	@Override
	public KnjigaDTO convert(Knjiga knjiga) {
		KnjigaDTO retVal = new KnjigaDTO();
		
		retVal.setId(knjiga.getId());
		retVal.setISBN(knjiga.getISBN());
		retVal.setNaziv(knjiga.getNaziv());
		retVal.setPisac(knjiga.getPisac());
		retVal.setIzdanje(knjiga.getIzdanje());
		retVal.setBrojGlasova(knjiga.getBrojGlasova());
		retVal.setIzdavacId(knjiga.getIzdavac().getId());
		retVal.setIzdavacNaziv(knjiga.getIzdavac().getNaziv());
		
		return retVal;
	}
	
	public List<KnjigaDTO> convert(List<Knjiga>  knjige) {
		
		List<KnjigaDTO> retVal = new ArrayList<>();
		
		for(Knjiga k : knjige) {
			
			retVal.add(convert(k));
		}
		
		return retVal;
	}

}
