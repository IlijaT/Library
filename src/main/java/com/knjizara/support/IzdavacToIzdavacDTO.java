package com.knjizara.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.knjizara.model.Izdavac;
import com.knjizara.web.dto.IzdavacDTO;

@Component
public class IzdavacToIzdavacDTO implements Converter<Izdavac, IzdavacDTO> {

	@Override
	public IzdavacDTO convert(Izdavac izdavac) {

		IzdavacDTO retVal = new IzdavacDTO();

		retVal.setId(izdavac.getId());
		retVal.setNaziv(izdavac.getNaziv());
		retVal.setAdresa(izdavac.getAdresa());
		retVal.setId(izdavac.getId());

		return retVal;
	}

	public List<IzdavacDTO> convert(List<Izdavac> izdavaci) {

		List<IzdavacDTO> retVal = new ArrayList<>();

		for (Izdavac x : izdavaci) {

			retVal.add(convert(x));
		}

		return retVal;
	}

}
