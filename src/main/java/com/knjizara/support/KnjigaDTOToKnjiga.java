package com.knjizara.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.knjizara.model.Knjiga;
import com.knjizara.service.IzdavacService;
import com.knjizara.service.KnjigaService;
import com.knjizara.web.dto.KnjigaDTO;

@Component
public class KnjigaDTOToKnjiga implements Converter<KnjigaDTO, Knjiga> {
	
	@Autowired
	KnjigaService knjigaSerrvice;
	
	@Autowired
	IzdavacService izdavacService;
	
	@Override
	public Knjiga convert(KnjigaDTO dto) {
		Knjiga retVal = new Knjiga();
		
		if(dto.getId() != null) {
			retVal = knjigaSerrvice.findOne(dto.getId());
			
			if(retVal == null) {
				throw new IllegalStateException("tried to modify a non-existant knjiga");							
			}
		}
		retVal.setId(dto.getId());
		retVal.setNaziv(dto.getNaziv());
		retVal.setPisac(dto.getPisac());
		retVal.setISBN(dto.getISBN());
		retVal.setIzdanje(dto.getIzdanje());
		retVal.setIzdavac(izdavacService.findOne(dto.getIzdavacId()));
		return retVal;
	}

}
