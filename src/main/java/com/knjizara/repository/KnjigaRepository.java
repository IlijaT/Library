package com.knjizara.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.knjizara.model.Knjiga;

@Repository
public interface KnjigaRepository extends JpaRepository<Knjiga, Long> {
	

	@Query("SELECT k FROM Knjiga k WHERE "
			+ "(:naziv IS NULL or k.naziv LIKE CONCAT('%', :naziv, '%') ) AND "
			+ "(:pisac IS NULL or k.pisac LIKE CONCAT('%', :pisac, '%') ) AND "
			+ "(:brojGlasova IS NULL OR k.brojGlasova <= :brojGlasova)"
			)
	Page<Knjiga> pretraga(
			@Param("naziv") String naziv, 
			@Param("pisac") String organizator, 
			@Param("brojGlasova") Integer brojGlasova,
			Pageable pageRequest);

	Page<Knjiga> findByIzdavacId(Long izdavacId, Pageable pageRequest);
	
	
//	SELECT * FROM knjiga WHERE 
//	broj_glasova = (SELECT max(broj_glasova) FROM knjiga);
	
	@Query("SELECT k FROM Knjiga k WHERE k.brojGlasova = (SELECT max(k.brojGlasova) FROM k)")
	List<Knjiga> najpopularnija();


}
