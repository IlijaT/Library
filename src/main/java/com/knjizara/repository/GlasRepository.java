package com.knjizara.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.knjizara.model.Glas;

@Repository
public interface GlasRepository extends JpaRepository<Glas, Long> {

}
