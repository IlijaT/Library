package com.knjizara.service;


import org.springframework.data.domain.Page;

import com.knjizara.model.Izdavac;

public interface IzdavacService {
	
	Izdavac findOne(Long id); 
	Page<Izdavac> findAll(int pageNum);
	Izdavac save(Izdavac izdavac); 
	void remove(Long id);

}
