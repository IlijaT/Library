package com.knjizara.service;


import java.util.List;

import org.springframework.data.domain.Page;

import com.knjizara.model.Knjiga;

public interface KnjigaService {
	
	Knjiga findOne(Long id); 
	Page<Knjiga> findAll(int pageNum);
	void  save(Knjiga knjiga); 
	void remove(Long id);
	
	Page<Knjiga> pretraga(String naziv, String pisac, Integer brojGlasova, int pageNum);
	
	Page<Knjiga> findByIzdavacId(Long id, int pageNum);
	
	List<Knjiga> najpopularnija();

}
