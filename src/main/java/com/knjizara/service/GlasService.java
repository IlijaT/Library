package com.knjizara.service;


import com.knjizara.model.Glas;

public interface GlasService {
	
	Glas glasaj(Long knjigaId); 

}
