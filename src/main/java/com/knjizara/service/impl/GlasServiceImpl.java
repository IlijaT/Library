package com.knjizara.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.knjizara.model.Glas;
import com.knjizara.model.Knjiga;
import com.knjizara.repository.GlasRepository;
import com.knjizara.service.GlasService;
import com.knjizara.service.KnjigaService;


@Service
public class GlasServiceImpl implements GlasService {
	
	@Autowired
	GlasRepository glasRepository;
	
	@Autowired
	private KnjigaService knjigaService;
	

	@Override
	public Glas glasaj(Long knjigaId) {
		
		Knjiga knjiga = knjigaService.findOne(knjigaId);
		
		if(knjiga == null) {
			throw new IllegalArgumentException("Id of a book cannot be null!");
		}
		
		knjiga.setBrojGlasova(knjiga.getBrojGlasova()+ 1);
		knjigaService.save(knjiga);
		
		Glas glas = new Glas();
		glas.setKnjiga(knjiga);
		
		glasRepository.save(glas);
		
		return glas;
	}

}
