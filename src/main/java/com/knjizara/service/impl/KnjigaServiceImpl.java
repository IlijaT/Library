package com.knjizara.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.knjizara.model.Knjiga;
import com.knjizara.repository.KnjigaRepository;
import com.knjizara.service.KnjigaService;

@Service
public class KnjigaServiceImpl implements KnjigaService{
	
	@Autowired
	private KnjigaRepository knjigaRepository;
	
	@Override
	public Knjiga findOne(Long id) {
		return knjigaRepository.findOne(id);
	}

	@Override
	public Page<Knjiga> findAll(int pageNum) {
		return knjigaRepository.findAll(new PageRequest(pageNum, 10));
	}

	@Override
	public void save(Knjiga knjiga) {
		knjigaRepository.save(knjiga);
	}
 

	@Override
	public Page<Knjiga> pretraga(String naziv, String organizator, Integer maxK, int page) {
		return knjigaRepository.pretraga(naziv, organizator, maxK, new PageRequest(page, 10));
	}

	 
	@Override
	public void remove(Long id) {
		knjigaRepository.delete(id);
		
	}

	@Override
	public Page<Knjiga> findByIzdavacId(Long id, int pageNum) {
		return knjigaRepository.findByIzdavacId(id, new PageRequest(pageNum, 10));
	}

	@Override
	public List<Knjiga> najpopularnija() {
		return knjigaRepository.najpopularnija();
	}
	
}
