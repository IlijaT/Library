package com.knjizara.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.knjizara.model.Izdavac;
import com.knjizara.repository.IzdavacRepository;
import com.knjizara.service.IzdavacService;

@Service
public class IzdavacServiceImpl implements IzdavacService {
	
	@Autowired
	IzdavacRepository izdavacRepository;

	@Override
	public Izdavac findOne(Long id) {
		return izdavacRepository.findOne(id);
	}

	@Override
	public Page<Izdavac> findAll(int pageNum) {
		return izdavacRepository.findAll(new PageRequest(pageNum, 10));
	}

	@Override
	public Izdavac save(Izdavac izdavac) {
		return izdavacRepository.save(izdavac);
		
	}

	@Override
	public void remove(Long id) {
		izdavacRepository.delete(id);
		
	}


	}


