package com.knjizara.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Knjiga {
	
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column(nullable=false)
	private String naziv;
	@Column(nullable=false)
	private Integer izdanje;
	@Column(nullable=false)
	private String pisac;
	@Column(unique=true)
	private String ISBN;
	@Column
	private Integer brojGlasova;
	@ManyToOne(fetch=FetchType.EAGER)
	private Izdavac izdavac;
	
	
	public Knjiga() {
		this.brojGlasova = 0;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public Integer getIzdanje() {
		return izdanje;
	}
	public void setIzdanje(Integer izdanje) {
		this.izdanje = izdanje;
	}
	public String getPisac() {
		return pisac;
	}
	public void setPisac(String pisac) {
		this.pisac = pisac;
	}
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}
	public Integer getBrojGlasova() {
		return brojGlasova;
	}
	public void setBrojGlasova(Integer brojGlasova) {
		this.brojGlasova = brojGlasova;
	}
	public Izdavac getIzdavac() {
		return izdavac;
	}
	public void setIzdavac(Izdavac izdavac) {
		this.izdavac = izdavac;
	}
	
	

}
